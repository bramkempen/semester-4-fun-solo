package models.repository;

import models.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StudentRepositoryTest {
    private StudentRepository studentRepository;
    private List<Student> students;

    @Before
    public void setUp() throws Exception {
        studentRepository = new StudentRepository();
        students = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            Student student = new Student();
            student.setEmail("test"+i+"@test.nl");
            student.setPassHash("myPassword");
            students.add(student);
        }
        studentRepository.save(students);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void findOne() {
        Student student = null;
        Student studentFromList = students.get(0);
        student = studentRepository.findOne(studentFromList.getId());

        assertEquals(student.getId(), studentFromList.getId());
    }

    @Test
    public void deleteWithId() {
    }

    @Test
    public void deleteWithEntity() {
    }


    @Test
    public void findAll() {
    }

    @Test
    public void save() {
    }
}