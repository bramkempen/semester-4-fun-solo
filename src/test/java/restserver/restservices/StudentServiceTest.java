package restserver.restservices;

import models.Student;
import models.repository.StudentRepository;
import org.junit.Before;
import org.junit.Test;
import restserver.handlers.StudentHandler;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

public class StudentServiceTest {

    private StudentHandler studentHandler;
    private StudentRepository studentRepository;
    private StudentService studentService;
    @Before
    public void setUp() throws Exception {
        studentRepository = new StudentRepository();
        studentHandler = new StudentHandler(studentRepository);
        StudentService.setHandler(studentHandler);
        studentService = new StudentService();


    }

    private Student addStudent(){
        Student student = new Student();
        student.setEmail("test@test.nl");
        student.setPassHash("secret");

        studentRepository.save(student);
        return student;
    }

    @Test
    public void byId() {
        Student student = addStudent();
        assertTrue(student.getId() > 0);

        Response result = studentService.byId(student.getId());
        assertEquals(200, result.getStatus());
    }

    @Test
    public void byIdNotExisting() {
        studentRepository.delete(5);
        Student student = studentRepository.findOne(5);
        assertNull(student);
        Response result = studentService.byId(5);
        assertEquals(404, result.getStatus());
    }
}