package models;


import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name="student")
public class Student implements BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String email;
    private String passHash;

    public Student() {
        feedpulses = new ArrayList<>();
    }

    public Student(String email, String passHash) {
        this.email = email;
        this.passHash = passHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String password) {
        // Hash a password for the first time
        this.passHash = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public boolean checkPassword(String password) {
        // Check that an unencrypted password matches one that has
        // previously been hashed
        return (BCrypt.checkpw(password, this.passHash));
    }


    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "account_id")
    private List<Feedpulse> feedpulses;

    public Student(String email, String passHash, List<Feedpulse> feedpulses) {
        this.setEmail(email);
        this.setPassHash(passHash);
        this.feedpulses = feedpulses;
    }

    public List<Feedpulse> getFeedpulses() {
        return Collections.unmodifiableList(feedpulses);
    }

    public void addFeedpulses(List<Feedpulse> feedpulses) {
        this.feedpulses.addAll(feedpulses);
    }

    public void addFeedpulse(Feedpulse feedpulse){
        this.feedpulses.add(feedpulse);
    }

    @Override
    public int getId() {
        return this.id;
    }
}
