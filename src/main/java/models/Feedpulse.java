package models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="feedpulse")
public class Feedpulse implements BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "feedpulse_id")
    @Fetch(FetchMode.SELECT)
    private List<Message> messages;

    public Feedpulse() {
    }

    public Feedpulse(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public int getId() {
        return id;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
