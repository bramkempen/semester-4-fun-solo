package models.repository;

import dbal.repository.AbstractRepository;
import models.Student;

public class StudentRepository extends AbstractRepository<Student, Integer> {
    public Class<Student> getDomainClass() {
        return Student.class;
    }
}
