package models.repository;

import dbal.repository.AbstractRepository;
import models.Feedpulse;

public class FeedpulseRepository extends AbstractRepository<Feedpulse, Integer> {
    public Class<Feedpulse> getDomainClass() {
        return Feedpulse.class;
    }
}
