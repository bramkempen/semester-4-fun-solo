package models.repository;

import dbal.repository.AbstractRepository;
import models.Admin;
import models.Feedpulse;

public class AdminRepository extends AbstractRepository<Admin, Integer> {
    public Class<Admin> getDomainClass() {
        return Admin.class;
    }
}
