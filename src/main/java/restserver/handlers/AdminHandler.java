package restserver.handlers;

import dbal.repository.IRepository;
import models.Admin;


public class AdminHandler extends AbstractHandler<Admin> implements IAdminHandler {

    public AdminHandler(IRepository repo) {
        super(repo);
    }
}
