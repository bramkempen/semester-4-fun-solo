package restserver.handlers;

import com.google.gson.Gson;
import dbal.repository.IRepository;
import models.Feedpulse;
import models.repository.FeedpulseRepository;
import restserver.response.Reply;


public class FeedpulseHandler extends AbstractHandler<Feedpulse> implements IFeedpulseHandler {

    public FeedpulseHandler(IRepository repo) {
        super(repo);
    }
}
