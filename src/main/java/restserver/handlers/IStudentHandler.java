package restserver.handlers;

import models.Student;

public interface IStudentHandler extends IHandler<Student> {
}
