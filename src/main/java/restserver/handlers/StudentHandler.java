package restserver.handlers;

import dbal.repository.IRepository;
import models.Student;


public class StudentHandler extends AbstractHandler<Student> implements IStudentHandler{

    public StudentHandler(IRepository repo) {
        super(repo);
    }
}
