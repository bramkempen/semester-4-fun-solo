package restserver;

import dbal.HibernateUtil;
import logging.Logger;
import models.repository.AdminRepository;
import models.repository.FeedpulseRepository;
import models.repository.StudentRepository;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.servlet.ServletContainer;
import restserver.handlers.AdminHandler;
import restserver.handlers.FeedpulseHandler;
import restserver.handlers.StudentHandler;
import restserver.restservices.AdminService;
import restserver.restservices.FeedpulseService;
import restserver.restservices.StudentService;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class RestService {

    public static void main(String[] args) {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        Server jettyServer = new Server(8070);

        //region Origin header
        FilterHolder cors = context.addFilter(CrossOriginFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin");
        //endregion

        jettyServer.setHandler(context);
        ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        FeedpulseHandler feedpulseHandler = new FeedpulseHandler(new FeedpulseRepository());
        FeedpulseService.setHandler(feedpulseHandler);

        AdminHandler adminHandler = new AdminHandler(new AdminRepository());
        AdminService.setHandler(adminHandler);

        StudentHandler studentHandler = new StudentHandler(new StudentRepository());
        StudentService.setHandler(studentHandler);
        // Tells the Jersey Servlet which REST service/class to load.

        jerseyServlet.setInitParameter("jersey.config.server.provider.packages",
                "restserver.restservices");

        System.out.println(HibernateUtil.initSessionFactory() ? "Hibernate has been initialized" : "Something went wrong with Hibernate");

        //test
        try {
            jettyServer.start();
            jettyServer.join();
        } catch (Exception e) {
            Logger.getInstance().log(e);
        } finally {
            jettyServer.destroy();
        }
    }
}
