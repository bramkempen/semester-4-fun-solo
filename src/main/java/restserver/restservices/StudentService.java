package restserver.restservices;

import models.Feedpulse;
import models.Student;
import restserver.handlers.IHandler;
import restserver.handlers.IStudentHandler;
import restserver.handlers.StudentHandler;
import restserver.response.Reply;
import restserver.response.Status;
import utils.GsonUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("/student")
public class StudentService {
    private static IStudentHandler handler;
    private Class<Student> entityClass = Student.class;

    public static void setHandler(IStudentHandler handler) {
        StudentService.handler = handler;
    }

    @GET
    @Path("/test")
    public Response test() {
        Reply reply = new Reply(Status.OK, "Test werkt echt");
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @GET
    @Path("/all")
    public Response all() {
        Reply reply = handler.getAll();
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @GET
    @Path("/{id}")
    public Response byId(@PathParam("id") int id) {
        Reply reply = handler.getById(id);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/save")
    @Consumes("application/json")
    public Response save(String data) {
        Student entity = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.save(entity);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/edit")
    @Consumes("application/json")
    public Response edit(String data) {
        Student entity = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.save(entity);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/remove")
    @Consumes("application/json")
    public Response remove(String data) {
        Student entity = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.remove(entity);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }
}