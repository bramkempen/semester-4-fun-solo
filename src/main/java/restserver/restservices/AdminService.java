package restserver.restservices;

import models.Admin;
import models.Student;
import restserver.handlers.AdminHandler;
import restserver.handlers.IAdminHandler;
import restserver.handlers.StudentHandler;
import restserver.response.Reply;
import utils.GsonUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("/admin")
public class AdminService {
    private static IAdminHandler handler;
    private Class<Admin> entityClass = Admin.class;

    public static void setHandler(IAdminHandler handler) {
        AdminService.handler = handler;
    }

    @GET
    @Path("/all")
    public Response all() {
        Reply reply = handler.getAll();
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @GET
    @Path("/{id}")
    public Response byId(@PathParam("id") int id) {
        Reply reply = handler.getById(id);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/save")
    @Consumes("application/json")
    public Response save(String data) {
        Admin entity = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.save(entity);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/edit")
    @Consumes("application/json")
    public Response edit(String data) {
        Admin entity = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.save(entity);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/remove")
    @Consumes("application/json")
    public Response remove(String data) {
        Admin entity = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.remove(entity);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }
}