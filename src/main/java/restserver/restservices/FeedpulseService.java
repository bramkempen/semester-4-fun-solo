package restserver.restservices;

import com.google.gson.Gson;
import models.Feedpulse;
import models.Student;
import restserver.handlers.FeedpulseHandler;
import restserver.handlers.IFeedpulseHandler;
import restserver.handlers.IHandler;
import restserver.response.Reply;
import utils.GsonUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("/feedpulse")
public class FeedpulseService {
    private static IFeedpulseHandler handler;
    private Class<Feedpulse> entityClass = Feedpulse.class;

    public static void setHandler(IFeedpulseHandler handler) {
        FeedpulseService.handler = handler;
    }

    @GET
    @Path("/all")
    public Response all() {
        Reply reply = handler.getAll();
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @GET
    @Path("/{id}")
    public Response byId(@PathParam("id") int id) {
        Reply reply = handler.getById(id);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/save")
    @Consumes("application/json")
    public Response save(String data) {
        Feedpulse feedpulse = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.save(feedpulse);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/edit")
    @Consumes("application/json")
    public Response edit(String data) {
        Feedpulse feedpulse = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.save(feedpulse);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }

    @POST
    @Path("/remove")
    @Consumes("application/json")
    public Response remove(String data) {
        Feedpulse feedpulse = GsonUtils.fromJson(data, entityClass);
        Reply reply = handler.remove(feedpulse);
        return Response.status(reply.getStatus().getCode()).entity(reply.getMessage()).build();
    }
}