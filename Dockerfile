FROM openjdk:8-jre
WORKDIR /
COPY target/fontys-1.0-SNAPSHOT-jar-with-dependencies.jar solo4.jar
EXPOSE 8070 8071
CMD java -jar solo4.jar